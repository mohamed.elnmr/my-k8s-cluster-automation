


resource "aws_launch_configuration" "worker-cluster" {
      name_prefix                 = "worker-cluster"
      image_id                    = "${var.ami_id}"
      instance_type               = "${var.instance_type}"
      security_groups             = ["${var.sg_ids}"]
      key_name                    = "${var.ssh_key}"
      associate_public_ip_address = "${var.associate_public_ip_address}"
  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "${var.volume_size}"
    delete_on_termination = true
  }


  


  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "worker-cluster-autoscale" {
  name                 = "worker-cluster"
  launch_configuration = "${aws_launch_configuration.worker-cluster.name}"
  min_size             = "${var.min_size}"
  max_size            = "${var.max_size}"
  vpc_zone_identifier = ["${var.subnet_id}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "worker-node"
    propagate_at_launch = true
  }
}


resource "aws_autoscaling_policy" "cpu-util" {
  name                   = "worker-cluster-terraform"
  policy_type            = "TargetTrackingScaling"
  target_tracking_configuration {
  predefined_metric_specification {
    predefined_metric_type = "ASGAverageCPUUtilization"
  }

  target_value = 60.0
}

  autoscaling_group_name = "${aws_autoscaling_group.worker-cluster-autoscale.name}"
}