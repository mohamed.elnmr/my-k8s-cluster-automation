resource "aws_instance" "bastion" {
count                     = "${var.num_of_instances}"
ami                         = "${var.ami_id}"
instance_type               = "${var.instance_type}"
subnet_id                   = "${var.subnet_id}"
vpc_security_group_ids      = ["${var.sg_ids}"]
key_name                    = "${var.ssh_key}"
associate_public_ip_address = "${var.associate_public_ip_address}"
  ebs_block_device {
device_name           = "/dev/sda1"
volume_size           = "${var.volume_size}"
delete_on_termination = true
  }

user_data = "${var.user_data}"

  tags {
Name        = "${var.cluster_name}-${var.instance_name}"
Environment = "${terraform.workspace}"
  }
  provisioner "file" {
    source = "${var.provision-path}"
    destination = "/home/ubuntu/"
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
  
  provisioner "file" {
    source = "${var.private-key-path}"
    destination = "/home/ubuntu/mint.pem"
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
  provisioner "remote-exec" {
    inline =[
      "chmod +x /home/ubuntu/ansible/provision/client-tools-bastion.sh",
      "chmod 400 /home/ubuntu/mint.pem",
      "sudo  /home/ubuntu/ansible/provision/client-tools-bastion.sh",

    ]
    connection{
        type        = "ssh"
        user        = "${var.user}"
        private_key = "${file("${var.private-key-path}")}"
    }
  }
}
