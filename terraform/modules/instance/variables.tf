variable "ami_id" {
  default ="ami-0dd655843c87b6930"
}
variable "num_of_instances" {
  default="1"
}
variable "instance_type" {}
variable "subnet_id" {}
variable "ssh_key" {}
variable "volume_size" {}
variable "cluster_name" {}
variable "instance_name" {}
variable "associate_public_ip_address" {}
variable "sg_ids" {
  type = "list"
}

variable "min_size" {
  default="2"
}
variable "max_size" {
  default="4"
}

variable "provision-path" {
  
}

variable "user" {
  default="ubuntu"
}

variable "private-key-path" {
  
}


